/* Copyright 2022 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */
#include <iostream>

#include <boost/property_tree/ptree.hpp>
namespace pt = boost::property_tree;
#include <boost/locale.hpp>

#include "../src/desktop.h"
#include "../src/coba_utils.h"

int parse() {
	desktop dt("./tests/test_dt.desktop", "alacritty -e");

	assert(dt.get_name() == "A name");
	assert(dt.get_icon() == "xterm");
	assert(! dt.get_no_display());
	assert(dt.get_categories().size() == 3);

	auto cats = dt.get_categories();
	
	for (auto cat : cats) {
		assert(
			cat == "Editors"
			|| cat == "Multimedia"
			|| cat == "Nil"
			);
	}

	return 0;
}

int render() {
	pt::ptree tree;

	desktop dt("./tests/test_dt.desktop", "kitty");

	dt.render(tree);

	std::string name = tree.get<std::string>("item.<xmlattr>.label");
	assert(name == "A name");

	std::string icon = tree.get<std::string>("item.<xmlattr>.icon");
	assert(icon == "xterm");

	std::string action = tree.get<std::string>("item.action.<xmlattr>.name");
	assert(action == "Execute");

	std::string exec = tree.get<std::string>("item.action.command");
	assert(exec == "kitty /bin/true");

	return 0;
}

int main(int argc, char* argv[]) {

	boost::locale::generator gen;
	std::locale::global(gen(""));

	if (argc < 2) {
		return parse() && render();
	}

	if (std::string(argv[1]) == "parse") {
		return parse();
	}
	if (std::string(argv[1]) == "render") {
		return render();
	}
	return 0;
}
