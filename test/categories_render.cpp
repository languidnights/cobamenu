#include <vector>
#include <iostream>

#include <boost/property_tree/ptree.hpp>
namespace pt = boost::property_tree;
#include <boost/locale.hpp>

#include "../src/coba_utils.h"
#include "../src/desktop.h"
#include "../src/categorize.h"

int render() {
	desktop dt("./tests/test_dt.desktop", "kitty");

	std::vector<desktop> dts;
	dts.push_back(dt);

	categories cats;
	cats.map_cats(dts);

	pt::ptree tree;
	cats.render(tree, false);

	int cats_count = 0;
	std::string valid_cats[] = {
		"Multimedia",
		"Editors"
	};

	for (auto elem : tree.get_child("openbox_pipe_menu")) {
		cats_count++;
		auto cat_tree = elem.second;
		std::string id_lookup("<xmlattr>.id");
		assert(cat_tree.get<std::string>(id_lookup).starts_with("cobamenu"));
		assert(cat_tree.get<std::string>(id_lookup).ends_with(valid_cats[0])
		       || cat_tree.get<std::string>(id_lookup).ends_with(valid_cats[1]));
	}

	assert(cats_count == 2);
	
	return 0;
}

int main(int argc, char* argv[]) {

	boost::locale::generator gen;
	std::locale::global(gen(""));

	if (argc < 2) {
		return render();
	}

	if (std::string(argv[1]) == "render") {
		return render();
	}
	
	return 0;
}
