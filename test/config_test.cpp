/* Copyright 2022 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */
#include "../src/coba_utils.h"

int test_config_location() {
	assert(get_config_file().find(".config") == std::string::npos);
	assert(get_config_file().find("tests") != std::string::npos);

	return 0;
}

int test_config_defaults() {
	assert(get_icon_theme() != "hicolor");
	assert(get_icon_theme() == "breeze");
	assert(get_terminal_emulator() != "kitty");
	assert(get_terminal_emulator() == "xterm");

	return 0;
}

int test_config_appdirs() {

	std::vector<std::string> dirs = get_application_dirs();
	assert(dirs.size() == 1);
	assert(dirs[0] == "./tests");

	assert(collect_desktops().size() == 1);

	return 0;
}

int main(int argc, char* argv[]) {

	if (argc < 2) {
		return test_config_location() == 0
			&& test_config_defaults() == 0
			&& test_config_appdirs() == 0;
	}

	if (std::string(argv[1]) == "location") {
		return test_config_location();
	}

	if (std::string(argv[1]) == "defaults") {
		return test_config_defaults();
	}

	if (std::string(argv[1]) == "app-dirs") {
		return test_config_appdirs();
	}

	assert(get_config_file().find(".config") == std::string::npos);
	assert(get_icon_theme() == "breeze");
	assert(get_terminal_emulator() == "xterm");

	std::vector<std::string> dirs = get_application_dirs();
	assert(dirs.size() == 1);
	assert(dirs[0] == "./tests");

	assert(collect_desktops().size() == 1);

	return 0;
}
