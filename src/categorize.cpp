/* Copyright 2022 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */

#include "categorize.h"

#include <iostream>
#include <set>
#include <string>
#include <vector>

#include <boost/property_tree/xml_parser.hpp>
namespace pt = boost::property_tree;

#include "desktop.h"
#include "icons.h"

void categories::map_cats(std::vector<desktop> &desks)
{
	const std::string categories[] = {
		"AudioVideo", "Multimedia", "Editors",	 "Engineering",
		"Games",      "Graphics",   "Internet",	 "Office",
		"Settings",   "System",	    "Utilities", "Other"
	};

	for (auto cat : categories) {
		this->cats[cat] = std::make_unique<category>(cat);
		this->cat_names.push_back(cat);
	}

	for (auto &dt : desks) {
		for (auto cat : dt.get_categories()) {
			auto it = cats.find(cat);
			if (it != cats.end()) {
				it->second->dtops.push_back(dt);
			}
		}
	}
}

std::vector<std::string> categories::get_cat_names() const
{
	return this->cat_names;
}

void categories::set_icon(const std::string &cat, const std::string &icon)
{
	auto it = cats.find(cat);
	if (it != cats.end()) {
		it->second->icon = icon;
	}
}

bool categories::render(pt::ptree &tree, bool static_menu)
{
	if (static_menu) {
		tree.put("menu.<xmlattr>.id", "xdg-root");
		tree.put("menu.<xmlattr>.label", "XDG Menu");
	}
	for (auto cat : this->get_cat_names()) {
		auto it = cats.find(cat);
		if (it == cats.end()) {
			continue;
		}
		pt::ptree inner_tree;
		bool any = it->second->render(inner_tree);
		if (any) {
			if (static_menu) {
				tree.add_child("menu.menu", inner_tree);
			} else {
				tree.add_child("openbox_pipe_menu.menu",
					       inner_tree);
			}
		}
	}
	return true;
}

bool categories::category::render(pt::ptree &tree)
{
	bool any = false;
	if (!dtops.empty()) {
		tree.add("<xmlattr>.id", std::string("cobamenu-") + name);
		tree.add("<xmlattr>.label", name);
		tree.add("<xmlattr>.icon", icon);

		std::map<std::string, bool> seen;
		for (auto &dt : dtops) {
			if (seen.find(dt.get_name()) == seen.end()) {
				if (!dt.get_no_display()) {
					dt.render(tree);
					any = true;
				}
				seen[dt.get_name()] = true;
			}
		}
	}
	return any;
}
