/* Copyright 2022 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */

#ifndef COBA_UTILS_H
#define COBA_UTILS_H

#include <string>
#include <vector>
#include <filesystem>

#include <boost/property_tree/xml_parser.hpp>
namespace pt = boost::property_tree;

std::string get_config_file();
std::string get_icon_theme();
std::string get_terminal_emulator();
std::vector<std::string> split_delim(std::string str, char delim);
std::string xml_escape(std::string);
std::vector<std::string> get_application_dirs();
std::vector<std::filesystem::path> collect_desktops();
std::string clean_xml(pt::ptree tree);
#endif
