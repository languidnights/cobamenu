/* Copyright 2022, 2024 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */

#include <vector>
#include <map>
#include <string>
#include <filesystem>
namespace fs = std::filesystem;
#include <iostream>

#include <boost/algorithm/string.hpp>

#include "icons.h"
#include "coba_utils.h"

std::map<std::string, fs::path> collect_icons(std::vector<fs::path>,
					      std::vector<std::string>);

icon_set::icon_set(bool icons)
{
	if (icons) {
		std::string home(getenv("HOME"));
		this->icon_roots.push_back(home + "/.icons/");
		this->icon_roots.push_back(home + "/.local/share/icons/");
		this->icon_roots.push_back(
			home + "/.local/share/flatpak/exports/share/icons/");
		this->icon_roots.push_back("/usr/local/share/icons/");
		this->icon_roots.push_back("/usr/local/share/pixmaps/");
		this->icon_roots.push_back("/usr/share/icons/");
		this->icon_roots.push_back("/usr/share/pixmaps/");

		std::vector<std::string> themes;
		themes.push_back(get_icon_theme());
		themes.push_back("hicolor");

		this->icon_paths = collect_icons(this->icon_roots, themes);
	}
}

std::string icon_set::get_icon(std::string name)
{
	std::string clean = boost::trim_copy(name);

	auto it = this->icon_paths.find(clean);
	if (it != this->icon_paths.end()) {
		return it->second.string();
	}

	return "";
}

void icon_set::cat_icon_list(categories *categories)
{
	std::string prefix("applications-");
	for (auto name : categories->get_cat_names()) {
		std::string lname = name;
		boost::to_lower(lname);

		std::string theme = get_icon_theme();
		std::string lookup = prefix + lname;
		if (theme.find("breeze") != std::string::npos) {
			if (lname == "development")
				lookup = "accessories-text-editor";
			if (lname == "editors")
				lookup = "accessories-text-editor";
			if (lname == "settings")
				lookup = "preferences-desktop";
			if (lname == "multimedia")
				lookup = "multimedia-player";
		}
		if (theme.find("Adwaita") != std::string::npos) {
			if (lname == "multimedia")
				lookup = "audio-speakers";
			if (lname == "editors")
				lookup = "accessories-text-editor";
			if (lname == "settings")
				lookup = "preferences-desktop";
			if (lname == "education")
				lookup = "accessories-dictionaries";
		}
		if (theme.find("Tango") != std::string::npos) {
			if (lname == "utilities")
				lookup = "applications-accessories";
		}
		categories->set_icon(name, get_icon(lookup));
	}
}

void collect_icons_from_path(fs::path path, auto &icons)
{
	const std::string icon_suffixes[] = { ".png", ".svg", ".xpm" };

	if (!fs::exists(path)) {
		return;
	}

	for (auto entry : fs::directory_iterator(path)) {
		for (auto suffix : icon_suffixes) {
			if (entry.path().extension() == suffix) {
				icons[entry.path().stem()] = entry.path();
			}
		}
	}
}

std::map<std::string, fs::path> collect_icons(std::vector<fs::path> paths,
					      std::vector<std::string> themes)
{
	std::map<std::string, fs::path> icons;

	const std::string sizes[] = {
		"symbolic", "apps",  "scalable", "512x512", "256x256",
		"128x128",  "16x16", "96x96",	 "72x72",   "64x64",
		"24x24",    "32x32", "36x36",	 "40x40",   "48x48",
		"16",	    "24",    "32",	 "48",	    "64"
	};
	const std::string icon_prefixes[] = {
		"legacy", "categories",	 "apps",    "devices", "mimetypes",
		"places", "preferences", "actions", "status",  "emblems"
	};

	for (auto theme : themes) {
		bool breeze = theme.find("breeze") != std::string::npos;
		bool oxygen = theme.find("oxygen") != std::string::npos;

		for (auto path : paths) {
			auto base = path.string() + theme + "/";
			if (path.string().find("pixmaps") > 0) {
				collect_icons_from_path(path, icons);
			}

			for (auto size : sizes) {
				for (auto prefix : icon_prefixes) {
					auto dirname =
						base + size + "/" + prefix;
					if (breeze) {
						dirname = base + prefix + "/" +
							  size;
					} else if (oxygen) {
						dirname = base + "/base/" +
							  size + "/" + prefix;
					}
					fs::path dir(dirname);
					collect_icons_from_path(dir, icons);
				}
			}
		}
	}
	return icons;
}
