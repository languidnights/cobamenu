/* Copyright 2022 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */

#ifndef COBA_CATEGORY_H
#define COBA_CATEGORY_H

#include <string>
#include <filesystem>
#include <vector>
#include <map>

#include <boost/property_tree/ptree.hpp>
namespace pt = boost::property_tree;

#include "desktop.h"

class categories {
    private:
	class category {
	    public:
		bool render(pt::ptree &tree);
		explicit category(const std::string &name)
		{
			this->name = name;
		}

		std::string name;
		std::string icon;
		std::vector<desktop> dtops;
	};

    public:
	void map_cats(std::vector<desktop> &desks);
	std::vector<std::string> get_cat_names() const;
	void set_icon(const std::string &cat, const std::string &icon);

	bool render(pt::ptree &tree, bool static_menu);

    private:
	std::vector<std::string> cat_names;
	std::map<std::string, std::unique_ptr<category>> cats;
};

#endif
