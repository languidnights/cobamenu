/* Copyright 2022 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include <iostream>
#include <fstream>
#include <vector>

#include <boost/property_tree/xml_parser.hpp>
namespace pt = boost::property_tree;
#include <boost/locale.hpp>

#include "coba_utils.h"
#include "desktop.h"
#include "icons.h"
#include "categorize.h"

int main(int argc, char *argv[])
{
	bool icons = false;
	bool static_menu = false;
	for (int i = 0; i < argc; i++) {
		if (strcmp(argv[i], "static") == 0)
			static_menu = true;
		if (strcmp(argv[i], "icons") == 0)
			icons = true;
	}

	icon_set iconset(icons);
	auto terminal = get_terminal_emulator();

	boost::locale::generator gen;
	std::locale::global(gen(""));

	auto desktops = collect_desktops();
	std::vector<desktop> dtops;
	for (auto dt : desktops) {
		desktop desk(dt, terminal);
		if (icons)
			desk.set_icon(iconset.get_icon(desk.get_icon()));
		else
			desk.set_icon("");
		dtops.push_back(std::move(desk));
	}

	categories cats;
	cats.map_cats(dtops);

	if (icons)
		iconset.cat_icon_list(&cats);

	pt::ptree tree;
	cats.render(tree, static_menu);
	auto output = clean_xml(tree);

	std::cout << output;

	return 0;
}
