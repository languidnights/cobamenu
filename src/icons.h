/* Copyright 2022 Christopher Nelson <christopher.nelson@languidnights.com> */
/* SPDX-License-Identifier: GPL-2.0-only */

#ifndef COBA_ICONS_H
#define COBA_ICONS_H

#include <vector>
#include <map>
#include <string>
#include <filesystem>
namespace fs = std::filesystem;

#include "desktop.h"
#include "categorize.h"

class icon_set {
    public:
	explicit icon_set(bool icons);
	void set_icon(desktop *desk_entry);
	std::string get_icon(std::string);

	void cat_icon_list(categories *categories);

    private:
	std::map<std::string, fs::path> icon_paths;
	std::vector<fs::path> icon_roots;
};

#endif
