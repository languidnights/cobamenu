Changelog
=========

All notable changes to *cobamenu* will be documented in this file.

The format is based on `Keep a Changelog`_ and this project adheres to
`Semantic Versioning`_

.. contents:: Releases
   :backlinks: top

2.2.0
----------

Changed
*******

* Add separators in LabWC menu generation script
* Rebrand, getting rid of awkward double-capitalized name
* Support some additional oddly-formatted .desktop files (for example, emacsclient and wesnoth) which use xml control characters and multiple escapes in them
* code reformat

Added
*****

* clang-format and ccls files for lsp, style, and formatting 
	       
2.1.0
----------

Changed
*******

* use full path to flatpak exports, allowing them to show in the menu
* fix XML compliance issue causing menu breakage on LabWC

Added
*****

* Added configuration options
  - exclude-application-dir: specify a location to not lookup .desktop files
  - merge-default-application-dirs: search config file and system application
    dirs for .desktop files [default=false]
* Example script for generating a LabWC menu from cobamenu output
    

2.0.1
-----

Changed
*******

* Slight documentation change (ChangeLog)

2.0.0
-----

Removed
*******

* No longer sources icon themes from gtk2 theme (gtk4 is now out)
* Checking if an executable exists before writing exec line
  - an empty executable is just as likely to run as a missing one

Changed
*******

* Adjusted desktop search path to put flatpaks ahead of system files
* Update required C++ version to 20

Added
*****

* Source configuration from XDG_CONFIG_HOME
* Added configuration options
  - application-dir: specify a location for .desktop files
  - terminal: specify which terminal emulator to use
  - icon-theme-name: specify which icon theme to use
* Jenkins CI integration
* CTest test suite
  - desktop parser
  - category parser
* SPDX headers

1.0.0
-----

Added
*****

* Generating `Openbox`_ compliant pipe menus from .desktop files
* Optionally attaching icons to the menu entries
* Using localized names if they exist in the .desktop file

.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html
.. _Openbox: http://openbox.org
