.. |License| image:: license-badge.svg
   :target: ./COPYING

.. |Status| image:: https://projects.languidnights.com/buildbot/badges/cobamenu.svg
   :target: https://projects.languidnights.com/buildbot

|License| |Status|

Cobamenu
--------

.. contents:: Contents
   :backlinks: top

About Cobamenu
==============

Cobamenu is a re-imagining of this Python3 version of `Obamenu`_. It
provides a right-click pipe_menu for `Openbox`_ which does a
dumb-but-quick pass over the user's and system's .desktop files.
Cobamenu is not, and does not claim to be, XDG-compliant! We're trading
quick-but-usable for truly compliant.

As the original Obamenu was a bit too slow for practical use on the
Raspberry Pi as a pipemenu, this project was envisioned to use a
compiled language to do the same thing.


Build Dependencies
==================

To build Cobamenu from source, you will need the following

* `CMake`_
* `cppcheck`_
* `boost`_


Installing
==========

To compile Cobamenu, it is recommended to do an out-of-tree build and to install to a non-system folder. The procedure would look something similar to:

.. code-block:: bash

  git clone git://languidnights.com/cobamenu
  cmake -DCMAKE_INSTALL_PREFIX=$HOME/.local -B build .
  cmake --build build
  cmake --install build

This, of course, assumes "$HOME/.local/bin" is in your $PATH and
"$HOME/.local/share/man" is in your $MANPATH, for example by setting in your
.bashrc

.. code-block:: bash

  PATH="$PATH:$HOME/.local/bin"
  MANPATH=":$HOME/.local/share/man"

Usage
=====

As a pipe menu
**************

In your .config/openbox/menu.rc file, comment any existing application
lines you don't want to keep (we recommend keeping the openbox-related
items), and them add the following into your root-menu

.. code-block:: xml

   <menu execute="~/bin/cobamenu" id="cobamenu" label="Cobamenu" />

If you would like pretty icons, at the cost of a slight increase in
processing time, set your favorite icon theme in .config/cobamenu/config
file. Cobamenu will search the 'hicolor' theme in all cases, as many apps
put their icons there only.

.. code-block:: INI

   icon-theme-name=breeze

and then use the "icons" flag to cobamenu

.. code-block:: xml

   <menu execute="~/bin/cobamenu icons" id="cobamenu" label="Cobamenu" />

Some applications require a terminal. Cobamenu defaults to `Alacritty`_,
but if you'd like to specify your own, set it in .config/cobamenu/config

.. code-block:: INI

   terminal=kitty

For application search directories, you can set the following to override
the default search paths

.. code-block:: INI

   # add /home/christopher/Apps to search path
   application-dir=/home/christopher/Apps
   # add /usr/local/share/applications to exclude path
   exclude-application-dir=/usr/local/share/applications
   # include default application search path instead of overriding
   merge-default-application-dirs=true

   
As a LabWC static menu
**********************

`LabWC`_ is a promising alternative to Openbox for those who prefer
Wayland. It re-uses much of the openbox configuration and menu syntax,
but does not (yet?) support pipe-menus.

There exists in the "scripts" folder an experimental script to put your
output as a static menu where LabWC will find it. This script will require
some modification for your own personal setup, but we've been using it
successfully for a little while now.

.. code-block:: bash

  ./scripts/menu_build

Contributing
============

Cobamenu isn't perfect, so if you have something to contribute, please
feel free! You can `email`_ the author; create an issue on the
`issue tracker`_; or go ahead, write the code yourself, and
`request a pull`_.

With such a small scope, we don't anticipate a huge amount of active
contributors, but if we reach three (3) or there's a problem, we'll
adopt an official Code of Conduct. Let's not make it a problem, eh?


Copying
=======

The original Obamenu of yore was licensed 3-Clause BSD for personal use,
and GPL v2.0 for non-personal use. Since this project isn't a single
person's copy modified for their home computer, this is released under a
`GPL 2.0`_ license (only).

.. _Openbox: http://openbox.org
.. _Obamenu: https://github.com/onuronsekiz/obamenu
.. _GPL 2.0: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
.. _CMake: https://cmake.org
.. _cppcheck: http://cppcheck.net
.. _boost: https://www.boost.org
.. _email: mailto:christopher.nelson+cobamenu@languidnights.com
.. _Alacritty: https://alacritty.org
.. _issue tracker: https://codeberg.org/languidnights/cobamenu/issues
.. _request a pull: https://codeberg.org/languidnights/cobamenu/pulls
.. _LabWC: https://github.com/labwc/labwc
